package jscrollidk;
import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import javax.swing.JTextArea;

import java.io.*;
public class MessangerClientThread extends Thread {
	 private Socket           socket   = null;
   private MessangerClient  client   = null;
   private DataInputStream  inStream = null;

   public MessangerClientThread(MessangerClient _client, Socket _socket) {
  	 client   = _client;
     socket   = _socket;
     open();  
     start();
   }
   public void open() {
  	 try {
  		 inStream = new DataInputStream(socket.getInputStream());
   		} catch(IOException ioe) {
  		 ioe.printStackTrace();
  		 client.stop();
  	 }
   }
   public void close() {
  	 try {
  		 if (inStream != null) inStream.close();
  	 } catch (IOException ioe) {
  		 ioe.printStackTrace();
  	 }
   }
   public void run() {
  	 while (true) {
  		 try {
  			 client.readMessage(inStream.readUTF());
  		 } catch (IOException ioe) {
  			 ioe.printStackTrace();
  			 client.stop();
  		 }
  	 }
   }
}
