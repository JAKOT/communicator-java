package jscrollidk;
import java.net.*;
import java.io.*;

public class MessangerServer implements Runnable {
	private MessangerServerThread clients[] = new MessangerServerThread[50];
  private ServerSocket 					server 		= null;
  private Thread       					thread		= null;
  private int 						 clientCount 		= 0;
  
  public MessangerServer(int port) {
  	try {
  		System.out.println("Binding to port " + port + ", please wait  ...");
  		server = new ServerSocket(port);
  		System.out.println("Server started: " + server);
  		start();
  	} catch (IOException ioe) {
  		ioe.printStackTrace();
  	}
  }
  public void run() {
  	while(thread != null) {
  		try {
  			System.out.println("Waiting for a client...");
  			addThread(server.accept());
  		} catch (IOException ioe) {
  			ioe.printStackTrace(); //restart/wyrzucenie clientów
  			for(MessangerServerThread cli : clients)
  				cli.interrupt();																	/// IDK CZY DOBRZE
  			this.stop();
  		}
  	}
  }
  
  public void start() {
  	if (thread == null) {
  		thread = new Thread(this);
  		thread.start();
  	}
  }
  public void stop() {
  	if (thread != null) {
  		thread.interrupt();          // TODO: Try Interrupt instead of stop
  		thread = null;
  	}
  }
  private int findClient(int ID) {
  	for (int i = 0; i < clientCount; i++)
      if (clients[i].getID() == ID)
         return i;
  	return -1;
  }
  
  public void addThread(Socket socket) {
  	if (clientCount < clients.length) {
  		System.out.println("Client accepted: " + socket);
  		clients[clientCount] = new MessangerServerThread(this, socket);
  		try {
  			clients[clientCount].open();
  			clients[clientCount].start();
  			clientCount++;
  		} catch (IOException ioe) {
  			ioe.printStackTrace();
  		}
  	} else System.out.println("Client refused: maximum " + clients.length + " reached.");
  }
  
  public synchronized void handleMessage(int ID, String input) throws InterruptedException {
  	if (input.equals(".end")) {
  		clients[findClient(ID)].send(".end");
  		remove(ID);
  	} else {
		 for (int i = 0; i < clientCount; i++) {
				clients[i].send(input);   
		 }
  	}
  }
  public synchronized void remove(int ID) {
  	int pos = findClient(ID);
  	if (pos >= 0) {
  		MessangerServerThread toTerminate = clients[pos];
  		System.out.println("Removing client thread " + ID + " at " + pos);
  		if (pos < clientCount-1) {
        for (int i = pos+1; i < clientCount; i++)
          clients[i-1] = clients[i];
  		}
  		clientCount--;
  		toTerminate.close();
  	}
  }
  public static void main(String[] args) {
  	MessangerServer server = null;
    if (args.length != 1)
       System.out.println("Usage: java MessangerServer <port>");
    else
       server = new MessangerServer(Integer.parseInt(args[0]));
	}
}
