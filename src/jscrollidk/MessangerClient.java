package jscrollidk;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultCaret;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.*;
import java.io.*;
import java.awt.*;
public class MessangerClient extends JFrame implements Runnable {
  static JPanel boxPanel = new JPanel();
	private Socket socket             		= null;
  private Thread thread             		= null;
  private DataInputStream  inStream  		= null;
  private DataOutputStream outStream 		= null;
  private MessangerClientThread client  = null;
  
  public MessangerClient(String nick) {
      initComponents();
      centerComponent(this);
      this.setResizable(false);
      jTextArea2.setLineWrap(true);
      nickname.setText(nick);
      String name = nickname.getText();
      boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.PAGE_AXIS));
      boxPanel.setVisible(true);
      this.setTitle("Communicator");
	    //System.out.println("Establishing connection. Please wait ...");
	      try {
	      	socket = new Socket("localhost", 8190);
	      	System.out.println("Connected: " + socket);
	      	addMessage("Connected: " + socket);
	      	start();
	      } catch(UnknownHostException uhe) {
	      	uhe.printStackTrace();
	      } catch (IOException ioe) {
	      	ioe.printStackTrace();
	      }
  }
  public void run() {
  	while(thread != null) {
  		try {
  			outStream.writeUTF(inStream.readLine());
  			outStream.flush();
  		} catch (IOException ioe) {
  			ioe.printStackTrace();
  			stop();
  		}
  	}
  }
  private void showMessage(final String msg) {
  	SwingUtilities.invokeLater( new Runnable() {
  		public void run() {
  			addMessage(msg);
  		}
  	});
  }
  //public void handle(String msg) {
  //		readMessage(msg);
  //}
  public void start() throws IOException {
  	inStream = new DataInputStream(System.in);
  	outStream = new DataOutputStream(socket.getOutputStream());

  	if (thread == null) {
  		client = new MessangerClientThread(this, socket);
  		thread = new Thread(this);
  		thread.start();
  	}
  }
  public void stop() {
  	if (thread != null) {
  		thread.stop();
  		thread = null;
  	}
  	try {
  		if (inStream   != null)  inStream.close();
      if (outStream != null)  outStream.close();
      if (socket    != null)  socket.close();
  	} catch(IOException ioe) {
  		ioe.printStackTrace();
  	}
    client.close();  
    client.stop();
  }

  private void centerComponent(Component frame) {
    Toolkit tk = Toolkit.getDefaultToolkit();
    Dimension dim = tk.getScreenSize();
    int xPos = (dim.width / 2) - (frame.getWidth() / 2);
    int yPos = (dim.height / 2) - (frame.getHeight() / 2);
    frame.setLocation(xPos, yPos);
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code"> 
  private void initComponents() {

      jScrollPane2 = new javax.swing.JScrollPane();
      jScrollPane3 = new javax.swing.JScrollPane();
      jList2 = new javax.swing.JList<>();
      jScrollPane4 = new javax.swing.JScrollPane();
      jTextArea1 = new javax.swing.JTextArea();
      menuBar1 = new java.awt.MenuBar();
      menu1 = new java.awt.Menu();
      menu2 = new java.awt.Menu();
      jMenuItem1 = new javax.swing.JMenuItem();
      jMenuBar2 = new javax.swing.JMenuBar();
      jMenu1 = new javax.swing.JMenu();
      jMenu2 = new javax.swing.JMenu();
      jMenuItem3 = new javax.swing.JMenuItem();
      textArea1 = new java.awt.TextArea();
      jScrollPane1 = new javax.swing.JScrollPane();
      jList1 = new javax.swing.JList<>();
      nickname = new javax.swing.JLabel();
      jScrollPane5 = new javax.swing.JScrollPane();
      jTextArea2 = new JTextArea();
      jScrollPane6 = new JScrollPane(boxPanel);
      jMenuBar1 = new javax.swing.JMenuBar();
      jMenu4 = new javax.swing.JMenu();
      jMenu6 = new javax.swing.JMenu();
      jMenu3 = new javax.swing.JMenu();
      jMenuItem4 = new javax.swing.JMenuItem();
      jMenu5 = new javax.swing.JMenu();
      
      SendButton = new javax.swing.JButton();
      SendButton.setText("Send");
      FileButton = new javax.swing.JButton();
      FileButton.setText("Add file");

      jList2.setModel(new javax.swing.AbstractListModel<String>() {
          String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
          public int getSize() { return strings.length; }
          public String getElementAt(int i) { return strings[i]; }
      });
      jScrollPane3.setViewportView(jList2);

      jTextArea1.setColumns(20);
      jTextArea1.setRows(5);
      jScrollPane4.setViewportView(jTextArea1);

      menu1.setLabel("File");
      menuBar1.add(menu1);

      menu2.setLabel("Edit");
      menuBar1.add(menu2);

      jMenuItem1.setText("jMenuItem1");

      jMenu1.setText("File");
      jMenuBar2.add(jMenu1);

      jMenu2.setText("Edit");
      jMenuBar2.add(jMenu2);

      jMenuItem3.setText("jMenuItem3");

      setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
      setMinimumSize(new java.awt.Dimension(600, 400));
      setSize(new java.awt.Dimension(700, 500));

      jList1.setBackground(new java.awt.Color(190, 190, 190));
      jList1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(210, 210, 210)));
      jList1.setModel(new javax.swing.AbstractListModel<String>() {
      	 String[] strings = { "Jakub", "Dawid", "Marcin", "Krzysztof", "Leszke", "Anotherone", "Contact", "Yo mom" , "Kopaczkens", "Karczi"};
          public int getSize() { return strings.length; }
          public String getElementAt(int i) { return strings[i]; }
      });
      jScrollPane1.setViewportView(jList1);

      nickname.setFont(new java.awt.Font("Ubuntu 15 Plain", 1, 16));
      nickname.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
      nickname.setText("MyNick");

      SendButton.addActionListener(new java.awt.event.ActionListener() {
          public void actionPerformed(java.awt.event.ActionEvent evt) {
              SendButtonActionPerformed(evt);
          }
      });
      SendButton.addKeyListener(new java.awt.event.KeyAdapter() {
          public void keyPressed(java.awt.event.KeyEvent evt) {
              SendButtonKeyPressed(evt);
          }
      });

      jTextArea2.setColumns(10);
      jTextArea2.setRows(3);
      jTextArea2.setWrapStyleWord(true);
      jTextArea2.setMinimumSize(new java.awt.Dimension(130, 30));
      jTextArea2.addKeyListener(new java.awt.event.KeyAdapter() {
          public void keyPressed(java.awt.event.KeyEvent evt) {
              jTextArea2KeyPressed(evt);
          }
      });
      jScrollPane5.setViewportView(jTextArea2);

      jScrollPane6.setBackground(new java.awt.Color(162, 174, 174));
      jScrollPane6.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      jScrollPane6.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
      jScrollPane6.setFocusable(false);
      jScrollPane6.setVerifyInputWhenFocusTarget(false);

      jMenu4.setText("Log out");
      jMenuBar1.add(jMenu4);

      jMenu6.setText("Settings");
      jMenuBar1.add(jMenu6);

      jMenu3.setText("Profile");

      jMenuItem4.setText("History");
      jMenuItem4.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					jMenuItem4ActionPerformed(evt);
				}
      	
      });
      
      jMenu3.add(jMenuItem4);

      jMenuBar1.add(jMenu3);

      jMenu5.setText("Add");
      jMenu5.addMouseListener(new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
              jMenu5MouseClicked(evt);
          }
      });
      
      jMenu4.addMouseListener(new MouseAdapter() {
      	public void mouseClicked(MouseEvent evt) {
      		jMenu4MouseClicked(evt);
      	}
      });
      
      jMenu6.addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent evt) {
            jMenu6MouseClicked(evt);
        }
    });
      jMenuBar1.add(jMenu5);

      setJMenuBar(jMenuBar1);

      javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
      getContentPane().setLayout(layout);
      layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(nickname, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
              .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                  .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(FileButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(SendButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                  .addGap(0, 4, Short.MAX_VALUE))
                .addComponent(jScrollPane6))))
          .addContainerGap())
      );
      layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
          .addContainerGap()
          .addComponent(nickname, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
              .addComponent(jScrollPane6)
              .addGap(18, 18, 18)
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createSequentialGroup()
                  .addComponent(SendButton)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(FileButton))))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
          .addGap(15, 15, 15))
      );

      pack();
  }// </editor-fold>                        

  private void SendButtonActionPerformed(ActionEvent evt) {                                        
      if(evt.getSource() == SendButton) {
      	addMessage();
      	//handle(jTextArea2.getText().trim());
      }
  }       
  
  private void jMenuItem4ActionPerformed(ActionEvent evt) {
  	HistoryFrame history = new HistoryFrame();
  	history.setVisible(true);
  }     
  
  private void jMenu4MouseClicked(MouseEvent evt) {
  	if(evt.getSource() == jMenu4) {
  		setVisible(false);
  		dispose();
  		LoginFrame login = new LoginFrame();
  		login.setVisible(true);
  	}
  }
    

  public void addMessage() {
    String msg = jTextArea2.getText().trim();
    if(msg.length()!=0) {
      JLabel label = new JLabel("[" + nickname.getText() + "] " + msg);
      label.setOpaque(true);
      label.setBackground(Color.lightGray);
      label.setText("<html><p style=\"width:400px\">" + label.getText() + "</p></html>");
      label.setForeground(Color.BLACK);
      boxPanel.add(label);
      boxPanel.add(Box.createRigidArea(new Dimension(0,5)));
      jTextArea2.setText("");
      boxPanel.revalidate();
      JScrollBar verticalBar = jScrollPane6.getVerticalScrollBar();
      AdjustmentListener downScroller = new AdjustmentListener() {
          public void adjustmentValueChanged(AdjustmentEvent e) {
              Adjustable adjustable = e.getAdjustable();
              adjustable.setValue(adjustable.getMaximum());
              verticalBar.removeAdjustmentListener(this); // need this so it does not stop user from scrolling up
          }
      };
      verticalBar.addAdjustmentListener(downScroller);
      sendMessage(msg);
    }
   }
	private void sendMessage(String message){
		try{
			outStream.writeUTF("[" + nickname.getText() + "] " + message);
			outStream.flush();
		}catch(IOException ioException){
			addMessage("\n Oops! Something went wrong!");
		}
	}
	public void readMessage(String msg) {
    JLabel label = new JLabel(msg);
    label.setOpaque(true);
    label.setBackground(Color.lightGray);
    label.setText("<html><p style=\"width:400px\">" + label.getText() + "</p></html>");
    label.setForeground(Color.BLACK);
    boxPanel.add(label);
    boxPanel.add(Box.createRigidArea(new Dimension(0,5)));
    jTextArea2.setText("");
    boxPanel.revalidate();
    JScrollBar verticalBar = jScrollPane6.getVerticalScrollBar();
    AdjustmentListener downScroller = new AdjustmentListener() {
        public void adjustmentValueChanged(AdjustmentEvent e) {
            Adjustable adjustable = e.getAdjustable();
            adjustable.setValue(adjustable.getMaximum());
            verticalBar.removeAdjustmentListener(this); // need this so it does not stop user from scrolling up
        }
    };
    verticalBar.addAdjustmentListener(downScroller);
	}
  
  public void addMessage(String _msg) {
    String msg = _msg.trim();
    if(msg.length()!=0) {
      JLabel label = new JLabel("[" + nickname.getText() + "] " + msg);
      label.setOpaque(true);
      label.setBackground(Color.lightGray);
      label.setText("<html><p style=\"width:400px\">" + label.getText() + "</p></html>");
      label.setForeground(Color.BLACK);
      boxPanel.add(label);
      boxPanel.add(Box.createRigidArea(new Dimension(0,5)));
      jTextArea2.setText("");
      boxPanel.revalidate();
      JScrollBar verticalBar = jScrollPane6.getVerticalScrollBar();
      AdjustmentListener downScroller = new AdjustmentListener() {
          public void adjustmentValueChanged(AdjustmentEvent e) {
              Adjustable adjustable = e.getAdjustable();
              adjustable.setValue(adjustable.getMaximum());
              verticalBar.removeAdjustmentListener(this); // need this so it does not stop user from scrolling up
          }
      };
      verticalBar.addAdjustmentListener(downScroller);
    }
   }
  
  private void SendButtonKeyPressed(KeyEvent evt) {                                   

  }                                  

  private void jTextArea2KeyPressed(KeyEvent evt) {                                      
      if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
      	addMessage();
      }
  }               

  private void jMenu5MouseClicked(MouseEvent evt) {                                    
      AddFrame add = new AddFrame();
      add.setVisible(true);
  }                                   

  private void jMenu6MouseClicked(MouseEvent evt) {
  	Settings settings = new Settings(nickname.getText());
  	settings.setVisible(true);
	}                                

  // Variables declaration - do not modify                     
  private JButton SendButton;
  private JButton FileButton;
  private javax.swing.JList<String> jList1;
  private javax.swing.JList<String> jList2;
  private javax.swing.JMenu jMenu1;
  private javax.swing.JMenu jMenu2;
  private javax.swing.JMenu jMenu3;
  private javax.swing.JMenu jMenu4;
  private javax.swing.JMenu jMenu5;
  private javax.swing.JMenu jMenu6;
  private javax.swing.JMenuBar jMenuBar1;
  private javax.swing.JMenuBar jMenuBar2;
  private javax.swing.JMenuItem jMenuItem1;
  private javax.swing.JMenuItem jMenuItem3;
  private javax.swing.JMenuItem jMenuItem4;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JScrollPane jScrollPane3;
  private javax.swing.JScrollPane jScrollPane4;
  private javax.swing.JScrollPane jScrollPane5;
  private javax.swing.JScrollPane jScrollPane6;
  private javax.swing.JTextArea jTextArea1;
  private javax.swing.JTextArea jTextArea2;
  private java.awt.Menu menu1;
  private java.awt.Menu menu2;
  private java.awt.MenuBar menuBar1;
  private javax.swing.JLabel nickname;
  private java.awt.TextArea textArea1;
  // End of variables declaration                   
}