package jscrollidk;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;

public class HistoryFrame extends javax.swing.JFrame {


  public HistoryFrame() {
    initComponents();
    centerComponent(this);
    this.setTitle("History");
  }
  
  private void centerComponent(Component frame) {
    Toolkit tk = Toolkit.getDefaultToolkit();
    Dimension dim = tk.getScreenSize();
    int xPos = (dim.width / 2) - (frame.getWidth() / 2);
    int yPos = (dim.height / 2) - (frame.getHeight() / 2);
    frame.setLocation(xPos, yPos);
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
  private void initComponents() {

    jPanel1 = new javax.swing.JPanel();
    jScrollPane1 = new javax.swing.JScrollPane();
    jList1 = new javax.swing.JList<>();
    jScrollPane2 = new javax.swing.JScrollPane();

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    setMaximumSize(new java.awt.Dimension(600, 400));
    setMinimumSize(new java.awt.Dimension(600, 400));
    setPreferredSize(new java.awt.Dimension(600, 400));
    setResizable(false);
    setSize(new java.awt.Dimension(600, 400));

    jPanel1.setMaximumSize(new java.awt.Dimension(600, 400));
    jPanel1.setMinimumSize(new java.awt.Dimension(600, 400));

    jList1.setModel(new javax.swing.AbstractListModel<String>() {
      String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
      public int getSize() { return strings.length; }
      public String getElementAt(int i) { return strings[i]; }
    });
    
    jList1.setModel(new javax.swing.AbstractListModel<String>() {
   	 String[] strings = { "Jakub", "Dawid", "Marcin", "Krzysztof", "Leszke", "Anotherone", "Contact", "Yo mom" , "Kopaczkens", "Karczi"};
       public int getSize() { return strings.length; }
       public String getElementAt(int i) { return strings[i]; }
   });
    jScrollPane1.setViewportView(jList1);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE))
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
      .addComponent(jScrollPane2)
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );

    pack();
  }// </editor-fold>                        

  // Variables declaration - do not modify                     
  private javax.swing.JList<String> jList1;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  // End of variables declaration                   
}
