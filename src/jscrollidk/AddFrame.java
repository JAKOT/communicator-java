package jscrollidk;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;


public class AddFrame extends javax.swing.JFrame {

  public AddFrame() {
      initComponents();
      centerComponent(this);
      this.setResizable(false);
      this.setTitle("Add Contact");

  }
  
  private void centerComponent(Component frame) {
    Toolkit tk = Toolkit.getDefaultToolkit();
    Dimension dim = tk.getScreenSize();
    int xPos = (dim.width / 2) - (frame.getWidth() / 2);
    int yPos = (dim.height / 2) - (frame.getHeight() / 2);
    frame.setLocation(xPos, yPos);
  }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
  private void initComponents() {

      jButton1 = new javax.swing.JButton();
      jLabel1 = new javax.swing.JLabel();
      jTextField1 = new javax.swing.JTextField();

      setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
      setTitle("Add contact");
      setAlwaysOnTop(true);

      jButton1.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
      jButton1.setText("+");
      jButton1.setMaximumSize(new java.awt.Dimension(30, 30));
      jButton1.setMinimumSize(new java.awt.Dimension(30, 30));
      jButton1.setPreferredSize(new java.awt.Dimension(30, 30));

      jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
      jLabel1.setText("Nick:");

      javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
      getContentPane().setLayout(layout);
      layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
          .addContainerGap()
          .addComponent(jLabel1)
          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE)
          .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addContainerGap())
      );
      layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
          .addGap(35, 35, 35)
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jLabel1)
            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addContainerGap(31, Short.MAX_VALUE))
      );

      pack();
  }// </editor-fold>                        

  private void button1ActionPerformed(java.awt.event.ActionEvent evt) {                                        
  }                                       

  // Variables declaration - do not modify                     
  private javax.swing.JButton jButton1;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JTextField jTextField1;
  // End of variables declarati                 
}
