package jscrollidk;
import java.net.*;
import java.io.*;
public class MessangerServerThread extends Thread {
	private MessangerServer 	server 		= null;
	private Socket 						socket 		= null;
	private int               ID        = -1;
	private DataInputStream   inStream  =  null;
	private DataOutputStream  outStream = null;
	private boolean 					flag 			= false; // Flaga warunkowa do działania wątku servera dla danego clienta
	
	public MessangerServerThread(MessangerServer _server, Socket _socket) {
		super();
		server = _server;
		socket = _socket;
		ID		 = socket.getPort();
		this.flag = true;
	}
	public void send(String msg) throws InterruptedException {
		try {
			outStream.writeUTF(msg);
			outStream.flush();
		} catch (IOException ioe) {
			ioe.printStackTrace();
 		 server.remove(ID);
 		 return;
		}
	}
	public int getID() { return ID; }
	public void run() {
		System.out.println("Server Thread " + ID + " running.");
		while (flag) { // flaga na false domyślnie
			try { 
				server.handleMessage(ID, inStream.readUTF());
			} catch (IOException ioe) {
				server.remove(ID);
				return; // return zamiast stop
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public void open() throws IOException {
 	 inStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
	 try {
		 outStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream())); //try catch jeżeli outstream rzuci wyjątek zamykamy instream
	 } catch (IOException ioe) {
		 ioe.printStackTrace();
		 inStream.close(); // zamykamy poniważ i tak nie będzie potrzebny skoro output failed
		 
	 }
	}
  public synchronized void close() {
  	flag = false;
    if (inStream != null)  {
    	try {
    		inStream.close(); 	// blokować - synchronized np. (nie mozęb yć współdzielone)
    	} catch (IOException ioe) {
    		
    	}
    }
    if (outStream != null) {
    	try {
    		outStream.close(); 	// blokować - synchronized np. (nie mozęb być współdzielone poniważ outputstream i inputstream są wykorzystywane przez wiele wątków)
    	} catch (IOException ioe) {
    		
    	}
    }
    if (socket != null) { //   socket.close();  	//obudować w try catch, pierw strumienie potem socket // 
	  	try {
	  		socket.close(); 	// blokować - synchronized np. (nie mozęb yć współdzielone)
	  	} catch (IOException ioe) {
	  		
	  	}
    }
    //this.join();
  }
}

