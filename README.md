## Autor: Jakub Kopyś Gr. Lab. 4 rok II IS WIMIiP ##

OPIS PROJEKTU:

Jest to komunikator pozwalający użytkownikowi na stworzenie konta, dodawaniu znajomych według ich nazw oraz komunikowanie się z nimi pojedynczo, lub w rozmowie grupowej. Dodatkowo komunikator ma pozwalać na przesyłanie plików takich jak obrazy .jpeg itp.

Harmonogram:

Przgotowanie GUI korzystając ze SWING:

* 	 Okno logowania oraz rejestracji                                  **X 15.03.2016** 
* 	 Okno główne komunikatora                                       **X 15.03.2016**
* 	 Okno ustawień/historii/dodawania kontaktów                        **X 15.03.2016**
* 	 Obsługa okien                                                      **X 15.03.2016**

 Zapis I odczyt plików:

* 	 zapisywanie błędów(np. wysyłanych przez kilenta serwerowi - logs) do pliku
* 	 Pobranie historii rozmów do pliki tekstowego
* 	 Zapisywanie listy kontaktów do pliku tekstowego
* 	 Dodawnie kontaktów z pliku tekstowego

Bazy danych:

* 	 zaprojektowanie bazy danych I implementacja połączenia z nią **!!!!!!**
* 	 dodawanie historii rozmów do bazy
* 	 rejestracja i logowanie do aplikacji **!!!!!!**
* 	 dodawanie kontaktów i wyświetlanie ich w liście

Współbieżność:

* 	synchronizacja współdzielnych obiektów - wspólne klasy do dzielenia watkow (np. skrzynnki)
* 	wielodostępowy server (writualne zebranie) **!!!!!!**
* 	sprawdzanie dostępnych kontaktów w osobnym wątku
* 	wątek do obsługi zegara - wątek u kilenta do kontroli przy kilku oodebranych wiadomościach (oczekiwanie na wiadomosci).

Połączenie Client/Server:

* 	stworzenie kilenta **X 05.04.2016r**
* 	stworzenie serwera **X 05.04.2016r**
* 	komunikacja kilent-serwer (rozmowa w trybie rozgłoszeniowym)
* 	rozmowy prywatne

Zaproponowane przez studenta:

* 	MVC
* 	powiadomienia o otrzymanej wiadomości
* 	grupy kontaktów
*       przesyłanie plików